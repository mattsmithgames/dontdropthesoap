using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DDTS
{
    public class ShowerController : MonoBehaviour
    {
   

        bool Activated = false;

        public GameObject targetGameObject;

        private void DoActivateTrigger()
        {
            if (targetGameObject != null)
            {
            targetGameObject.SetActive(true);
            }

        }

        private void DoDeActivateTrigger()
        {
            if (targetGameObject != null)
            {
                targetGameObject.SetActive(false);
                
            }

        }


        private void OnTriggerEnter(Collider other)
        {
            if(!Activated)
            {
                Debug.Log("trigger");
                DoActivateTrigger();
                Activated = true;
            }

        }

        private void OnTriggerExit(Collider other)
        {
            if(Activated)
            {
            Debug.Log("Exit");
            DoDeActivateTrigger();
            Activated = false;
            }
        }
    }
}
