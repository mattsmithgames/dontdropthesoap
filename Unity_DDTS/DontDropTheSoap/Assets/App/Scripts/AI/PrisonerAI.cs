using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace DDTS
{

    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof(PrisonerMovement))]

    public class PrisonerAI : MonoBehaviour
    {


        public UnityEngine.AI.NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public PrisonerMovement character { get; private set; } // the character we are controlling
        public Transform target;                                    // target to aim for
        public float ShowerTime = 10f;
        public bool lost = false;
        public bool showering = false;
        public bool showered = false;
        public bool complete = false;

        NavMeshObstacle NMO;
        

        public bool repath = false;

        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<PrisonerMovement>();

            agent.updateRotation = false;
            agent.updatePosition = true;

            NMO = transform.GetComponent<NavMeshObstacle>();
            NMO.enabled = false;


        }


        private void Update()
        {

            if (!agent.isActiveAndEnabled)//retrun if not active agent
                return;

            if (target != null && repath )
            {
                //Debug.Log("AI set path");
                 agent.SetDestination(target.position);
               // StartCoroutine(CalcPath());
                repath = false;
                return;
            }

            else if (agent.remainingDistance > agent.stoppingDistance)
            {
                // Debug.Log("AI move"); 
                character.Move(agent.desiredVelocity, false, false);
                lost = false;
                if (agent.pathStatus == NavMeshPathStatus.PathPartial)
                {
                    if (target.tag == "shower")//stop and repath to a new shower
                    {
                        //Debug.Log("lost shower");
                        imLost();
                    }


                }
                return;
            }
            else
            {
                OnStopped();
            }




        }


        void OnStopped()
        {
           // Debug.Log("AI stop");
            character.Move(Vector3.zero, false, false);
            if (complete)
                return;

            if (target != null)
            {
                if (target.gameObject.tag == "Shower")
                {
                    //Debug.Log("checkSHower");
                    //check distance
                    if (TargetDistance(target) <= 0.5f)      
                    {//in shower
                        StartCoroutine(Shower());
                        target = null;
                    }
                    else
                    {
                        imLost();
                    }
                }

                else if (target.gameObject.name == "AIExit")
                {
                    //Debug.Log("checkExit");
                    if (TargetDistance(target) <= 0.5f)
                    {
                        //Debug.Log("exitcomplete");
                        complete = true;
                        target = null;
                    }
                    else
                    {
                        imLost();
                    }
                }
                else
                {
                    imLost();
                }   

            }

        }

        void imLost()
        {
            if (agent.pathPending)
                return;

            Debug.Log("Im lost  " + transform.name);
            lost = true;
            target = null;
            agent.ResetPath();

        }

        float TargetDistance(Transform t)
        {
            //remove height distance first
            Vector3 a = new Vector3(t.position.x, 0f, t.position.z);
            Vector3 b = new Vector3(transform.position.x, 0f, transform.position.z);
            float d = 0f;
            d = Vector3.Distance(a, b);
            return d;
        }

        IEnumerator CalcPath()
        {
            lost = false;
            yield return new WaitForEndOfFrame();
           bool a =  agent.SetDestination(target.position);
            // Debug.Log(a);
        //  yield return new WaitForEndOfFrame();
         Debug.Log(agent.pathStatus);
    }



        void MakeObstacle(bool state)
        {
           // Debug.Log("make  obstacle" + state);
            if (state)
            {
                //Debug.Log("made obstacle");
                agent.enabled = false;
                NMO.enabled = true;
            }
            else
            {
               // Debug.Log("made agent");
                NMO.enabled = false;
                agent.enabled = true;
                if (showered)
                    agent.avoidancePriority = 1;
            }
        }


        public void SetTarget(Transform t)
        {
            lost = false;
            target = t;
            repath = true;
        }

        IEnumerator Shower()
        {
            Debug.Log("startShower");

            yield return new WaitForSeconds(0.1f);
            showering = true;
            character.DoShower( showering);
            MakeObstacle(true);
            yield return new WaitForSeconds(ShowerTime);
            showered = true;
            showering = false;
            character.DoShower(showering);
            MakeObstacle(false);
            yield return new WaitForEndOfFrame();
        }
    }
}
