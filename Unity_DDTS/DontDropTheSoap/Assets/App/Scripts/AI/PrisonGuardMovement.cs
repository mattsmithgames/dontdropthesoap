using UnityEngine;

namespace DDTS
{
	[RequireComponent(typeof(Rigidbody))]
	[RequireComponent(typeof(CapsuleCollider))]
	[RequireComponent(typeof(Animator))]
	public class PrisonGuardMovement : MonoBehaviour
	{


		Rigidbody m_Rigidbody;
		Animator m_Animator;
		
        float timer = 0f;

        void Start()
		{


            //	m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
            m_Animator = gameObject.GetComponent<Animator>();

            timer = Time.fixedTime + Random.Range(0f,20f);
		}


        private void Update()
        {

            if ( timer < Time.fixedTime)
            {
                TriggerLook();
                timer = Time.fixedTime + Random.Range(2f, 20f);
            }

            
        }


        void TriggerLook()
        {
            m_Animator.SetTrigger("Look");

        }




	}
}
