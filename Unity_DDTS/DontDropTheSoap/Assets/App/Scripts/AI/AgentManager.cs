﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DDTS
{
    public class AgentManager : MonoBehaviour
    {
        public int maxAgents = 20;
        public float spawnDelay = 3f;
        public GameObject[] agentsCatalogue;
        public List<PrisonerAI> agentsSpawned = new List<PrisonerAI>();

        GameObject startLoc;
        GameObject endLoc;
        GameObject[] showerLoc;
        GameObject AIparent;
        float spawnTimer;


        // Use this for initialization
        void Start()
        {
            startLoc = GameObject.Find("AIStart");
            endLoc = GameObject.Find("AIExit");
            showerLoc = GameObject.FindGameObjectsWithTag("Shower");
            AIparent = GameObject.Find("AGENTS");
            spawnTimer = spawnDelay;
            SpawnAgent();
        }

        // Update is called once per frame
        void Update()
        {
            if (spawnTimer < Time.fixedTime)//spawn a charater
            {
                SpawnAgent();
                spawnTimer = Time.fixedTime + spawnDelay;
            }

            UpdateActiveAgents();

        }

        void SpawnAgent()
        {
            if (agentsSpawned.Count >= maxAgents)
                return;

            int rand = Random.Range(0, agentsCatalogue.Length);
            GameObject ai = Instantiate(agentsCatalogue[rand], startLoc.transform.position, Quaternion.identity, AIparent.transform);
            PrisonerAI cc = ai.GetComponent<PrisonerAI>();
            cc.SetTarget(SelectShower());
            agentsSpawned.Add(cc);;
            ai.name = Random.Range(0, 99999).ToString();
        }

       void UpdateActiveAgents()
        {

            for (int i = 0; i < agentsSpawned.Count; i++) 
            {

               // Debug.Log(   "checked   " + agentsSpawned[i].name);

                if (agentsSpawned[i].showering )// return ealry
                {
                    //Debug.Log("im showering" + agentsSpawned[i].name);
                }
                else if(agentsSpawned[i].lost && !agentsSpawned[i].showered)//is lost and need a new shower 
                {

                    agentsSpawned[i].SetTarget(SelectShower());
                   // Debug.Log("find new  shower" + agentsSpawned[i].name);
                }

                else if (agentsSpawned[i].target == null && agentsSpawned[i].showered && !agentsSpawned[i].complete)//has showered and needs an exit
                {
                    //Debug.Log("set exit" + agentsSpawned[i].name);
                    Transform t = endLoc.transform;
                    agentsSpawned[i].SetTarget(t);
                }
                else if(agentsSpawned[i].complete)//has completed and needs to be destroyed
                {
                    //Debug.Log("destroy" + agentsSpawned[i].name);
                    DestroyImmediate(agentsSpawned[i].transform.gameObject);
                    agentsSpawned.Remove(agentsSpawned[i]);
                    
                   
                }
            }
        }

        Transform SelectShower()
        {

            Transform target = showerLoc[Random.Range(0, showerLoc.Length) ].transform;
            return target;
        }
    }
}
