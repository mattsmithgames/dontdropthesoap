﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwing : MonoBehaviour {

    public float maxAnglePerSwingX= 2f;
    public float maxAnglePerSwingZ = 2f;
    public float maxSpinPerSecond = 40f;
    public float swingTime = 0f;
    public bool spinEnable = true;

    float spin = 0f;
    float swingX = 0f;
    float swingZ = 0f;
    bool positive = true;

    float rate;

	// Use this for initialization
	void Start () {

        swingX = Random.Range(0f, maxAnglePerSwingX);
        swingZ = Random.Range(0f, maxAnglePerSwingZ);
        spin = Random.Range(-maxSpinPerSecond, maxSpinPerSecond);
        SetStartAngle();
        rate = 1f / swingTime;
        StartCoroutine(Swing());


    }


    IEnumerator Swing()
    {

        float angleX = swingX;// * Mathf.Deg2Rad;
        float angleZ = swingZ;// * Mathf.Deg2Rad;
        float angleY = 0f;
        if (!positive)
        {
            angleX = 0 - swingX;// * Mathf.Deg2Rad;
            angleZ = 0 - swingZ;// * Mathf.Deg2Rad;
        }

        if (spinEnable)
            angleY = spin;// * Mathf.Deg2Rad;


        Quaternion currentRotation = transform.localRotation;
        Quaternion newRotation = currentRotation * Quaternion.Euler(angleX, angleY, angleZ);

       // Debug.Log(currentRotation.eulerAngles);
        //Debug.Log(newRotation.eulerAngles);

       //Debug.Log("complete  " + swingTime);
        float t = 0f;
        while (t <= 1f)
        {
           // Debug.Log(t);
            transform.rotation = Quaternion.Slerp(currentRotation, newRotation,  Mathf.SmoothStep(0.0f, 1.0f, t));
            t += rate * Time.deltaTime; 
            yield return new WaitForEndOfFrame();

            
        }
        yield return new WaitForEndOfFrame();
        positive = !positive;

        
        StartCoroutine(Swing());

        

    }

    void SetStartAngle()
    {
        transform.Rotate(Vector3.left, swingX/2);
        transform.Rotate(Vector3.back, swingZ/2);
    }

}
