﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LighFlicker : MonoBehaviour {

    float randomTimeDelay;
    Light light;
	// Use this for initialization
	void Start () {
        GetNextTime();
        light = transform.GetComponent<Light>();
    }
	
	// Update is called once per frame
	void Update () {

		if (randomTimeDelay < Time.fixedTime)
        {
            GetNextTime();
            if (Random.value > 0.7f)
                StartCoroutine( Flicker());
        }
	}

    void GetNextTime()
    {
        randomTimeDelay = Time.fixedTime + Random.Range(10f, 120f);
    }

    IEnumerator Flicker()
    {
        int flickerCount = Random.Range(2, 8);
        float intensity = light.intensity;

        while (flickerCount > 0)
        {
            --flickerCount;
            light.intensity = 0;
            yield return new WaitForSeconds(Random.Range(0.05f, 0.2f));
            light.intensity = Random.Range(0f, intensity);
            yield return new WaitForSeconds(Random.Range(0.05f, 0.2f));
            
        }

        light.intensity = intensity;
    }



}
