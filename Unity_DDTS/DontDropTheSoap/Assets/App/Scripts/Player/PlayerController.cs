using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;

//MODIFIED FROM UNITY  ThirdPersonUserControl

namespace DDTS
{
    [RequireComponent(typeof (PlayerMovement))]
    public class PlayerController : MonoBehaviour
    {
        private PlayerMovement character; // A reference to the ThirdPersonCharacter on the object
        private float forward;
        private float rotate ;
        private bool jump;                      // the world-relative desired move direction, calculated from the camForward and user input.
        private bool crouch;  
        private Vector2 hand;  
       // public Text textForward;
//        public Text textRotate;
        private void Start()
        {
            
            
            // get the  player character ( this should never be null due to require component )
            character = GetComponent<PlayerMovement>();
        }



        // Fixed update is called in sync with physics
        private void FixedUpdate()
        {
            // read inputs
            rotate = CrossPlatformInputManager.GetAxis("Horizontal");
            forward = CrossPlatformInputManager.GetAxis("Vertical");
            crouch = CrossPlatformInputManager.GetButton("Crouch");
            jump = CrossPlatformInputManager.GetButtonDown("Jump");
            //Debug.Log(crouch + "crouch" );
            hand = new Vector2(CrossPlatformInputManager.GetAxis("MouseX"), CrossPlatformInputManager.GetAxis("MouseY"));
            
             //walk speed multiplier
            if (Input.GetKey(KeyCode.LeftShift)) 
            {
                forward *= 1.5f;
            }

            // pass all parameters to the character control script
            character.Move(forward, rotate, crouch, jump, hand);
          //  textForward.text = "forward = " + forward.ToString();
           // textRotate.text = "rotate = " + rotate.ToString();

        }
    }
}
