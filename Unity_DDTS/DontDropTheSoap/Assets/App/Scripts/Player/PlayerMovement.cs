using UnityEngine;
using UnityEngine.UI;

namespace DDTS
{
	[RequireComponent(typeof(Rigidbody))]
	[RequireComponent(typeof(CapsuleCollider))]
	//[RequireComponent(typeof(Animator))]
	public class PlayerMovement : MonoBehaviour
	{
        [SerializeField] float walkForce = 500; //newtons - seems to get us 2m/s for an 80kg
        [SerializeField] float walkSpeed = 1.6f; //m/s
        [SerializeField] float runMulti = 2f; //multi of the walk for a jog
		[SerializeField] float backMulti = 0.5f; //multi of the walk slow down
        [SerializeField] float jumpPower = 12f;
		[SerializeField] float rotateDegreesPerSec = 2f;
        [SerializeField] float animSpeedMultiplier = 1.0f;
        [SerializeField]float runCycleLegOffset = 1.0f;
        [SerializeField]float groundCheckDistance = 2f;
		[SerializeField]float rotateForceWalk = 25f;
		[SerializeField]float rotateForceStill = 60f;

		Vector3 moveDirection;
        Rigidbody playerRB;
		Rigidbody handRB;
		Animator animator;
		public bool isGrounded;
		const float half = 0.5f;
		float rotateAmount;
		float forwardAmount;
		Vector3 groundNormal;
		float capsuleHeight;
		Vector3 capsuleCenter;
		CapsuleCollider capsule;
		bool crouching;
        Vector3 lastPos;	
        float desiredSpeed = 0f;
        float actualSpeed = 0f;
	//	public Text textCrntSpeed;
	//	public Text textMaxSpeed;
		//public Text textForce;
		Vector3 originalLocalPosition;	
		public  Transform head;//object that camera is on
		[SerializeField] private AudioSource AS;
		//[SerializeField] private Transform HandObject;
		[SerializeField] private Transform handRotateCentre;
		Quaternion handRotateStart;

		ConfigurableJoint handJoint;

		Vector3 lastPlayerMovement;
        void Start()
		{
			playerRB = GetComponent<Rigidbody>();
			capsule = GetComponent<CapsuleCollider>();
			capsuleHeight = capsule.height;
			capsuleCenter = capsule.center;
			playerRB.constraints = RigidbodyConstraints.FreezeRotationX  | RigidbodyConstraints.FreezeRotationZ;//RigidbodyConstraints.FreezeRotationY
			playerRB.angularDrag = 3f;
			playerRB.drag = 0f;
			playerRB.mass = 80f;
            lastPos = transform.position;

			//headbob
			originalLocalPosition = head.localPosition;
			//hand move
			
			handRB = handRotateCentre.GetComponent<Rigidbody>();
			handRotateStart = transform.rotation;
			handJoint = gameObject.GetComponent<ConfigurableJoint>();
		}


		public void Move(float forward, float rotate, bool crouch, bool jump, Vector2 hand)// caled in fixedupdate
		{

			actualSpeed = playerRB.velocity.magnitude;
			CheckGroundStatus();
            moveDirection = Vector3.ProjectOnPlane(transform.forward, groundNormal);//align direction with ground normal
            rotateAmount = rotate;
            forwardAmount = forward;

			ScaleCapsuleForCrouching(crouch);

			PreventStandingInLowHeadroom();

            // send input and other state parameters to the animator
			if(forwardAmount > 0f)
			{
				desiredSpeed = walkSpeed;
				moveDirection = transform.forward;
			}
			else if(forwardAmount < 0f)
			{
				desiredSpeed = walkSpeed * backMulti;
				moveDirection = -moveDirection;//reverse
			}
			else
			{
				desiredSpeed = 0f;
				moveDirection = Vector3.zero;
			}

            MovePlayer(desiredSpeed);

			RotatePlayer(rotateAmount);

			//MoveHand(hand);

			Headbob();

			DynamicFootstep();

			//update move offset
		}

		void MovePlayer(float desiredSpeed)
        {        
			//MOVE PLAYER
			
            //calc speed required
            float maxSpeed = desiredSpeed;
            if (Input.GetKey(KeyCode.LeftShift))
			{
                maxSpeed = runMulti * desiredSpeed; 
			}
			
		//	textMaxSpeed.text = "Desired Speed = " + maxSpeed.ToString();
			
			//create force to move
			if(maxSpeed > 0)
			{
			//check if speed is reached in forward direction normalized
			if(Mathf.Abs(playerRB.velocity.sqrMagnitude) <= (maxSpeed * maxSpeed))
				{
					AddForce();
				}
			// slow down to 0 by applying opposit force, try to top slipping
			} else if(playerRB.velocity.sqrMagnitude > 0.05f)
			{
				RemoveForce(); //essentially drag 
			} else 
			{
				//Debug.Log("nothing happening");
				//textForce.text = "Force = " + 0;
			}
			//textCrntSpeed.text = "Speed = " + actualSpeed;

        	lastPlayerMovement = playerRB.position - lastPos;

			
        }

		void RotatePlayer(float rotate)//using torgee
		{
			//change rotatet force based on velocity
			float t = actualSpeed/(walkSpeed * runMulti);
			float force = Mathf.Lerp(rotateForceStill,rotateForceWalk,t);
			playerRB.AddTorque(transform.up * force * rotate);
		//	Debug.Log(force + "   " + t);

		}

		void AddForce() 
		{
			////Debug.Log("speed up");
			//consider which way player want to move 
			Vector3 force = moveDirection * walkForce;
			playerRB.AddForce(force, ForceMode.Force);
			//textForce.text = "Force = " + force.ToString();
		}

		void RemoveForce() //slowdown player
		{
		//	Debug.Log("slow down");
			Vector3 force = -playerRB.velocity.normalized * walkForce * backMulti;
			playerRB.AddForce(force, ForceMode.Force);

			//textForce.text = "Force = " + force.ToString();
		}

		[SerializeField] private float handXDistance = 0.4f;
		[SerializeField] private float handYDistance = 0.4f;
		[SerializeField] private float handZDistance = 0.2f;
		Quaternion deltaX = new Quaternion();
		Quaternion deltaY = new Quaternion();

		float targetAngleX = 0f;
		float targetAngleY = 0f;
		Quaternion targetRotation;
		void MoveHand(Vector2 hand)
		{

			float angleX = hand.x * 500f;
			float angleY = hand.y * 500f;

			targetAngleX += angleY * Time.fixedDeltaTime;
			targetAngleY += angleX * Time.fixedDeltaTime;
			//addX
    		targetRotation = Quaternion.AngleAxis( -targetAngleX, Vector3.right) * Quaternion.AngleAxis( targetAngleY, Vector3.forward) ;
    		handJoint.targetRotation = targetRotation;
		}

		void ScaleCapsuleForCrouching(bool crouch)
		{
	
		//	Debug.Log(crouch + " " + isGrounded);
			if (isGrounded && crouch)
			{
				if (crouching) return;
				capsule.height = capsule.height / 2f;
				capsule.center = capsule.center / 2f;

				//transform.localScale = new Vector3(1f,0.5f,1f);
				crouching = true;
				//Debug.Log("crouched");
			}
			else
			{
				Ray crouchRay = new Ray(playerRB.position + Vector3.up * capsule.radius * half, Vector3.up);
				float crouchRayLength = capsuleHeight - capsule.radius * half;
				if (Physics.SphereCast(crouchRay, capsule.radius * half, crouchRayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore))
				{
					crouching = true;
					return;
				}
				capsule.height = capsuleHeight;
				capsule.center = capsuleCenter;
				//transform.localScale = new Vector3(1f,1f,1f);
				crouching = false;
				//Debug.Log("NOTcrouched");
			}
		}

		void PreventStandingInLowHeadroom()
		{
			// prevent standing up in crouch-only zones
			if (!crouching)
			{
				Ray crouchRay = new Ray(playerRB.position + Vector3.up * capsule.radius * half, Vector3.up);
				float crouchRayLength = capsuleHeight - capsule.radius * half;
				if (Physics.SphereCast(crouchRay, capsule.radius * half, crouchRayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore))
				{
					crouching = true;
				}
			}
		}

		//Head bob
        float yPos = 0;
        float xPos = 0;
        float zTilt = 0;
        float xTilt = 0;
        float bobSwayFactor;
        float bobFactor;
        float strideLangthen;
        float flatVel;
		float  springPosition = 0;
		Vector3  previousVelocity = new Vector3(0,0,0);
		float springVelocity = 0;
		float springElastic = 1.1f;
		float springDampen = 0.8f;
		float springVelocityThreshold = 0.05f;
		float springPositionThreshold = 0.05f;
		float headbobCycle = 0;
		float headbobFade = 0f;
		public float headbobFrequency = 1.5f;
		public float headbobSpeedMultiplier = 3f;
		public float headbobSideMovement = 1f;
		public float headbobHeight = 5f;
		public float headbobSwayAngle = 5f;
		
		
         void Headbob()
		{
            Vector3 vel = (playerRB.position - lastPos) / Time.fixedDeltaTime;
            Vector3 velChange = vel - previousVelocity;
		//	Debug.Log(velChange);
            
            lastPos = playerRB.position;
            previousVelocity = vel;
            springVelocity -= velChange.y;
            springVelocity -= springPosition * springElastic;
            springVelocity *= springDampen;
            springPosition += springVelocity * Time.fixedDeltaTime;
            springPosition = Mathf.Clamp(springPosition, -0.3f, 0.3f);

            if(Mathf.Abs(springVelocity) < springVelocityThreshold && Mathf.Abs(springPosition) < springPositionThreshold) { springPosition = 0; springVelocity = 0; }
            flatVel = new Vector3(vel.x, 0.0f, vel.z).magnitude;
            strideLangthen = 1 + (flatVel * ((headbobFrequency*2)/10));
            headbobCycle += (flatVel / strideLangthen) * (Time.fixedDeltaTime / headbobFrequency);
            bobFactor = Mathf.Sin(headbobCycle * Mathf.PI * 2);
            bobSwayFactor = Mathf.Sin(Mathf.PI * (2 * headbobCycle + 0.5f));
            bobFactor = 1 - (bobFactor * 0.5f + 1);
            bobFactor *= bobFactor;

            yPos = 0;
            xPos = 0;
            zTilt = 0;
            xTilt = -springPosition;

            if(isGrounded)
            {
                if(new Vector3(vel.x, 0.0f, vel.z).magnitude < 0.1f) { headbobFade = Mathf.MoveTowards(headbobFade, 0.0f, Time.deltaTime); } else { headbobFade = Mathf.MoveTowards(headbobFade, 1.0f, Time.deltaTime); }
                float speedHeightFactor = 1 + (flatVel * (headbobSpeedMultiplier/10));
                xPos = -(headbobSideMovement/10) * bobSwayFactor;
                yPos = springPosition +  bobFactor * (headbobHeight/10) * headbobFade * speedHeightFactor;
                zTilt = bobSwayFactor * (headbobSwayAngle/10) * headbobFade;

				//Debug.Log(xPos + "  " + yPos);
            }

			//set vars
			head.localPosition = originalLocalPosition + new Vector3(xPos, yPos, 0);
        	head.localRotation = Quaternion.Euler(xTilt, 0, zTilt);

        }


    	[SerializeField] private AudioClip[] footStepSounds;
		[SerializeField] private AudioClip landSound;
		[SerializeField] private AudioClip jumpSound;
		bool previousGrounded = false;
		private float nextStepTime = 0.5f;
		[SerializeField] private float Volume = 1f;
        void DynamicFootstep()
        {
			if(isGrounded)
			{
				if(!previousGrounded)
				{
					AS.PlayOneShot(landSound,Volume/10); 
					nextStepTime = headbobCycle + 0.5f;
				} else
				{
					if(headbobCycle > nextStepTime)
					{
						nextStepTime = headbobCycle + 0.5f;
						int n = Random.Range(0, footStepSounds.Length);
						AS.PlayOneShot(footStepSounds[n],Volume/10);
						
					}
				}
				previousGrounded = true;
			}   
		} 


		void CheckGroundStatus()
		{
			RaycastHit hitInfo;
#if UNITY_EDITOR
			// helper to visualise the ground check ray in the scene view
			Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * groundCheckDistance));
#endif
			// 0.1f is a small offset to start the ray from inside the character
			// it is also good to note that the transform position in the sample assets is at the base of the character
			if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, groundCheckDistance))
			{
				groundNormal = hitInfo.normal;
				isGrounded = true;
				//Debug.Log("ground");
			//	animator.applyRootMotion = true;
			}
			else
			{
				isGrounded = false;
				groundNormal = Vector3.up;
				//Debug.Log("NOTground");
			///	animator.applyRootMotion = false;
			}
		}
	}
}
