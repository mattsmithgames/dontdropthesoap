﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotate : MonoBehaviour {


    public float angleFrom = 0f;
    public float angleTo = 90f;
    public float direction = 1f;


    float fullAngle = 0f;
    public float anglePerSec = 10f;



	
	// Update is called once per frame
	void Start () {
        
        fullAngle = Mathf.Abs(angleFrom - angleTo);
        StartCoroutine(Rotate());
    }


    IEnumerator Rotate()
    {
        transform.localRotation = Quaternion.Euler(0, angleFrom, 0);

        float angleMoved = 0f;


        while (angleMoved <= fullAngle)
        {
            angleMoved += anglePerSec * Time.deltaTime;
            transform.Rotate(Vector3.up, anglePerSec * direction * Time.deltaTime );
            //Debug.Log("pos  " + angleMoved);
            yield return new WaitForEndOfFrame();
            
        }
        //Debug.Log("half");
        yield return new WaitForSeconds(0.2f);



        while (angleMoved >= 0)
        {
            angleMoved -= anglePerSec * Time.deltaTime;
            transform.Rotate(Vector3.up, anglePerSec * -direction * Time.deltaTime);
            //Debug.Log("neg   " + angleMoved);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(0.2f);
        StartCoroutine(Rotate());


    }
}
