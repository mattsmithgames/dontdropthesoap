// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

//Based on below
// MatCap Shader, (c) 2015-2017 Jean Moreno

Shader "DDTS/Toon/Bumped Textured Multiply"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_BumpMap ("Normal Map", 2D) = "bump" {}
		_MatCap ("MatCap (RGB)", 2D) = "white" {}
		_Color ("Base Color", Color) = (1,1,1,1)	
	}
	
	Subshader
	{
	
		Pass
		{
			Name "BASE"
			Tags {"LightMode"="ForwardBase" "RenderType"="Opaque" }
			LOD 250
			ZWrite On
	   		Cull Back
			Lighting On

			CGPROGRAM
				
				#pragma vertex vert
				#pragma fragment frag
				//#pragma fragmentoption ARB_precision_hint_fastest
				//#pragma multi_compile_fwdbase	
				#pragma multi_compile_fog
				#pragma multi_compile_fwdadd_fullshadows

				#include "UnityCG.cginc"
				#include "Lighting.cginc"
				#include "UnityLightingCommon.cginc"
				#include "UnityShaderVariables.cginc"
				#include "AutoLight.cginc"
				
				struct v2f
				{
					
					float2 uv : TEXCOORD0;
					SHADOW_COORDS(1)
					fixed3 diff : COLOR0;
					fixed3 ambient : COLOR1;
					float2 uv_bump : TEXCOORD2;
					float4 pos	: SV_POSITION;
					float3 c0 : TEXCOORD3;
					float3 c1 : TEXCOORD4;
					UNITY_FOG_COORDS(5)

				};
				
				uniform float4 _MainTex_ST;
				uniform float4 _BumpMap_ST;
				
				v2f vert (appdata_tan v)
				{
					v2f o;
					o.pos = UnityObjectToClipPos (v.vertex);
					o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
					o.uv_bump = TRANSFORM_TEX(v.texcoord,_BumpMap);
					
					v.normal = normalize(v.normal);
					v.tangent = normalize(v.tangent);
					TANGENT_SPACE_ROTATION;
					o.c0 = mul(rotation, normalize(UNITY_MATRIX_IT_MV[0].xyz));
					o.c1 = mul(rotation, normalize(UNITY_MATRIX_IT_MV[1].xyz));

					half3 worldNormal = UnityObjectToWorldNormal(v.normal);
					half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
					o.diff = nl * _LightColor0.rgb;

					o.ambient = ShadeSH9(half4(worldNormal,1));


					UNITY_TRANSFER_FOG(o, o.pos);
					// compute shadows data
					TRANSFER_SHADOW(o)
					return o;
				}
				
				uniform sampler2D _MainTex;
				uniform sampler2D _BumpMap;
				uniform sampler2D _MatCap;
				//uniform sampler2D _Color;
				
				fixed4 frag (v2f i) : COLOR
				{
					fixed4 tex = tex2D(_MainTex, i.uv);
					// compute shadow attenuation (1.0 = fully lit, 0.0 = fully shadowed)
					fixed shadow = SHADOW_ATTENUATION(i);
					fixed3 normals = UnpackNormal(tex2D(_BumpMap, i.uv_bump));
					// darken light's illumination with shadow, keep ambient intact
					fixed3 lighting =  i.diff *  shadow + i.ambient; 

					half2 capCoord = half2(dot(i.c0, normals), dot(i.c1, normals));
					float4 col = tex  ;//* tex2D(_MatCap, capCoord*0.5+0.5) * tex;
					
					
				    col.rgb *= lighting;


					UNITY_APPLY_FOG(i.fogCoord, mc);


					return col;
				}
			ENDCG
		}
	}
	
	FallBack "Diffuse"
}
