// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "DDTS/Toon/Outline"
{
    Properties 
    {

		_MainTex ("Detail", 2D) = "white" {}        							
		//_BumpMap ("Normal Map", 2D) = "bump" {}
		_MatCap ("MatCap (RGB)", 2D) = "white" {}	
		_Color ("Base Color", Color) = (1,1,1,1)									    
		_Brightness ("Brightness 1 = neutral", Float) = 1.0		
		
		_OutlineColor ("Outline Color", Color) = (0.5,0.5,0.5,1.0)					
		_Outline ("Outline width", Float) = 0.4

		
    }
 
    SubShader
    {
        Tags { "RenderType"="Opaque" }
		LOD 250 

        UsePass "DDTS/Toon/Bumped Textured Multiply/BASE"
       
   	
        Pass
        {

            Cull Front
            ZWrite On
            CGPROGRAM
			//#pragma fragmentoption ARB_precision_hint_fastest
			#pragma glsl_no_auto_normalization
            #pragma vertex vert
 			#pragma fragment frag
			#include "UnityCG.cginc"
			
			
            struct appdata_t 
            {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f 
			{
				float4 pos : SV_POSITION;
			};

            fixed _Outline;

            
            v2f vert (appdata_t v) 
            {
                v2f o;
			    o.pos = v.vertex;
			    o.pos.xyz += normalize(v.normal.xyz) *_Outline*0.01;
			    o.pos = UnityObjectToClipPos(o.pos);
			    return o;
            }
            
            fixed4 _OutlineColor;
            
            fixed4 frag(v2f i) :COLOR 
			{
		    	return _OutlineColor;
			}
            
            ENDCG
        }

		
    }
	FallBack "Diffuse"
}